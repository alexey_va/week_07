package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import ru.edu.model.SymbolImpl;
import ru.edu.model.Symbol;

import java.time.Instant;

public class SymbolPriceServiceImpl implements SymbolPriceService {
    /**
     * Текст сообщения.
     */
    private static final String HTTP_SERVICE_ERROR = "Ошикбка сервиса {}";
    /**
     * Текст сообщения.
     */
    private static final String HTTP_SERVICE_OK = "Сервис вызван {}. Ответ: {}";
    /**
     * Текст сообщения.
     */
    private String apiUrl =
            "https://api.binance.com/api/v3/ticker/price?symbol=";

    /**
     * Текст сообщения.
     */
    private RestTemplate restTemplate = new RestTemplate();

    /**
     * Текст сообщения.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(SymbolPriceServiceImpl.class);

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого
     * данные будут обновляться не чаще, чем раз в 10 секунд.
     *
     * @param symbolName
     * @return
     */
    @Override
    public Symbol getPrice(final String symbolName) {
        String url = apiUrl + symbolName;

        try {
            ResponseEntity<SymbolImpl> response = restTemplate
                    .getForEntity(url, SymbolImpl.class);
            if (response.getStatusCode() != HttpStatus.OK) {
                LOGGER.error(HTTP_SERVICE_ERROR, response.getStatusCode());
                return null;
            }
            LOGGER.debug(HTTP_SERVICE_OK, symbolName, response.getStatusCode());

            SymbolImpl symbol = response.getBody();
            symbol.setTimeStamp(Instant.now());

            return symbol;

        } catch (RestClientException ex) {
            ex.getStackTrace();
            return null;
        }



    }
}
