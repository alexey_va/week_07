package ru.edu;

public final class Main {
    /**
     * Конструткор.
     */
    private Main() {
        //not called
    }

    /**
     * Запуск потоков мониоринга курсов валют.
     * @param args
     */
    public static void main(final String[] args) {
        SymbolPriceService service = new SymbolPriceServiceImpl();
        SymbolPriceService cached = new CashedSymbolServiceImpl(service);

        String[] symbols = {"BNBBTC", "ETHBTC", "LTCBTC"};

        Thread[] threads = new Thread[symbols.length];

        for (int i = 0; i < symbols.length; i++) {
            threads[i] = new Thread(new Watcher(symbols[i], cached));
            threads[i].start();
        }


    }
}
