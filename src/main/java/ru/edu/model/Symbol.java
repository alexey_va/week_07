package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

/**
 * Интерфейс информации о курсе обмена.
 */
public interface Symbol {
    /**
     * Получить симбол.
     * @return Симбол.
     */
    String getSymbol();

    /**
     *  Прайс.
     * @return прайс
     */
    BigDecimal getPrice();

    /**
     * Время получения данных.
     *
     * @return время
     */
    Instant getTimeStamp();
}
