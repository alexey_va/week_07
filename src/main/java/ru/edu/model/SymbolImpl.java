package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

public class SymbolImpl implements Symbol {
    /**
     * symbol.
     */
    private String symbol;

    /**
     * price.
     */
    private BigDecimal price;

    /**
     * timeStamp.
     */
    private Instant timeStamp;

    /**
     * getSymbol.
     * @return String
     */
    @Override
    public final String getSymbol() {
        return symbol;
    }
    /**
     * getPrice.
     * @return BigDecimal
     */
    @Override
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Время получения данных.
     *
     * @return
     */
    @Override
    public Instant getTimeStamp() {
        return timeStamp;
    }

    /**
     * setSymbol.
     * @param newSymbol
     */
    public void setSymbol(final String newSymbol) {
        this.symbol = newSymbol;
    }

    /**
     * setPrice.
     * @param myPrice
     */
    public void setPrice(final BigDecimal myPrice) {
        this.price = myPrice;
    }

    /**
     * setTimeStamp.
     * @param myTimeStamp
     */
    public void setTimeStamp(final Instant myTimeStamp) {
        this.timeStamp = myTimeStamp;
    }

    /**
     * toString.
     * @return String
     */
    @Override
    public String toString() {
        return "SymbleImpl{"
                + "symbol='" + symbol + '\''
                + ", price=" + price
                + ", timeStamp=" + timeStamp
                + '}';
    }
}
