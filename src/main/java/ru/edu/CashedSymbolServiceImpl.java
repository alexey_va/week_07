package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.edu.model.Symbol;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CashedSymbolServiceImpl implements SymbolPriceService {
    /**
     * Текст создания кэша.
     */
    private static final String CASH_CREATE = "Кэш создан";
    /**
     * Текст о статусе обновления каша.
     */
    private static final String CASH_ERROR = "Кэш не обнолен";
    /**
     * Время обновления кэша.
     */
    public static final int TIMEOUT = 10;

    /**
     * Фабрика логов.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(CashedSymbolServiceImpl.class);
    /**
     * Сервис кэшированния.
     */
    private final SymbolPriceService delegete;


    /**
     * Кэш для хранения курсов валют.
     */
    private Map<String, Symbol> cache =
            Collections.synchronizedMap(new HashMap<>());

    /**
     * Конструктор.
     * @param symbolService
     */
    public CashedSymbolServiceImpl(final SymbolPriceService symbolService) {
        this.delegete = symbolService;
    }



    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого данные будут
     * обновляться не чаще, чем раз в 10 секунд.
     *
     * @param symbolName
     * @return Symbol
     */
    @Override
    public Symbol getPrice(final  String symbolName) {
        synchronized (symbolName) {
            if (!cache.containsKey(symbolName)
                    || Instant.now().minus(TIMEOUT, ChronoUnit.SECONDS).isAfter(
                    cache.get(symbolName).getTimeStamp())) {
                LOGGER.debug(CASH_CREATE);
                Symbol price = delegete.getPrice(symbolName);

                cache.put(symbolName, price);

            }
            LOGGER.debug(CASH_ERROR);
        }
        return cache.get(symbolName);    }
}
