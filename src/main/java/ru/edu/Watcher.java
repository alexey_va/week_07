package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.edu.model.Symbol;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;

public class Watcher implements Runnable {
    /**
     * Текст сообщения.
     */
    private static final  String RESPONSE_OK = "Ответ: {}";
    /**
     * Текст сообщения.
     */
    private static final String LOG_WRITE_ERROR =
            "Ошибка записи лога в файл. Текст ошибки {}";
    /**
     * Логи.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(Watcher.class);

    /**
     * Символ для отслеживания.
     */
    private final String symbolToWatch;

    /**
     * service.
     */
    private final SymbolPriceService service;

    /**
     * Время актуальности информации в кэше.
     */
    public static final long TIMESLEEP = 5_000L;

    /**
     * Количество итераций.
     */
    public static final int ITERATOR = 7;

    /**
     * Конструтор.
     * @param symboltmp
     * @param servicetmp
     */
    public Watcher(final String symboltmp,
                   final SymbolPriceService servicetmp) {
        this.symbolToWatch = symboltmp;
        this.service = servicetmp;
    }
    /**
     * When an object implementing interface {@code Runnable} is used
     * to create a thread, starting the thread causes the object's
     * {@code run} method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method {@code run} is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        int i = ITERATOR;

        Symbol lastSymbol = null;

        String fileName = "priceHistory/" + symbolToWatch + ".txt";

        while (i > 0) {
            --i;

            String distinct;

            Symbol symbol = service.getPrice(symbolToWatch);

            if (symbol != null) {
                if (lastSymbol == null) {
                    distinct = "0";
                } else {
                    BigDecimal oldPrice = lastSymbol.getPrice();
                    BigDecimal newPrice = symbol.getPrice();
                    BigDecimal delta = newPrice.subtract(oldPrice);
                    distinct = delta.toPlainString();
                }

                String str = symbol.toString() + "\t" + distinct;

                printToLog(fileName, str);

                LOGGER.debug(RESPONSE_OK, symbol);

                lastSymbol = symbol;
            }

            try {
                Thread.sleep(TIMESLEEP);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Запись в лог.
     * @param file
     * @param str
     */
    public void printToLog(final String file, final String str) {

        try {

            FileWriter writer = new FileWriter(file, true);
            BufferedWriter bufferWriter = new BufferedWriter(writer);
            bufferWriter.write(str + "\n");
            bufferWriter.close();

        } catch (IOException ex) {
            LOGGER.error(LOG_WRITE_ERROR,
                    ex.getStackTrace());
        }
    }
}
