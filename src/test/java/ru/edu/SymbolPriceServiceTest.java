package ru.edu;

import org.junit.Test;
import org.springframework.web.client.RestTemplate;
import ru.edu.model.Symbol;

import java.time.Instant;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SymbolPriceServiceTest {

    public static final String SYMBOL_NAME = "SomeVal";

    private CashedSymbolServiceImpl service;

    private Symbol mockSymbol1 = mock(Symbol.class);
    private Symbol mockSymbol2 = mock(Symbol.class);



    /**
     * для тестирование через прямую реализацию.
     */
    private SymbolPriceServiceImpl symbolPriceService = new SymbolPriceServiceImpl();
    private CashedSymbolServiceImpl cashedSymbolService = new CashedSymbolServiceImpl(symbolPriceService);


    @Test
    public void getPrice() throws InterruptedException {

        /**
         * примерный тест проверки работы кэша, делаем 2 вызова и смотрим что данные были получены в одно время
         * после ожидания таймаута жизни данных в кэше делаем повторный запрос и проверяем, что данные имеют метку времени,
         * полученную после сброса.
         */

        service = mock(CashedSymbolServiceImpl.class);

        when(service.getPrice(SYMBOL_NAME)).thenReturn(mockSymbol1, mockSymbol2);
        when(mockSymbol1.getTimeStamp()).thenReturn(Instant.now());
        when(mockSymbol2.getTimeStamp()).thenReturn(Instant.now());

        Symbol price = service.getPrice(SYMBOL_NAME);
        Symbol priceAgain = service.getPrice(SYMBOL_NAME);

        Thread.sleep(10_000L);

        Symbol priceNew = service.getPrice(SYMBOL_NAME);

        when(priceNew.getTimeStamp()).thenReturn(Instant.now());

        Instant timeStamp1 = price.getTimeStamp();
        Instant timeStamp2 = priceNew.getTimeStamp();

        assertTrue(timeStamp1.isBefore(timeStamp2));
    }


    /**
     * тестирование через прямую реализацию.
     */
    @Test
    public void getPriceRestApi() throws InterruptedException {

        Symbol price = cashedSymbolService.getPrice("BNBBTC");
        Symbol priceAgain = cashedSymbolService.getPrice("BNBBTC");

        Instant timeStamptmp1  = price.getTimeStamp();
        Instant timeStamptmp2  =  priceAgain.getTimeStamp();

        assertEquals(timeStamptmp1, timeStamptmp2);

        System.out.println("Ожидание очистки кэша");
        Thread.sleep(15_000L);

        Symbol priceNew = cashedSymbolService.getPrice("BNBBTC");

        assertTrue(price.getTimeStamp().isBefore(priceNew.getTimeStamp()));

    }

}