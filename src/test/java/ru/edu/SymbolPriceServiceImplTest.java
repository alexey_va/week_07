package ru.edu;

import junit.framework.TestCase;
import ru.edu.model.Symbol;

public class SymbolPriceServiceImplTest extends TestCase {
    public void testGetPrice() {

        SymbolPriceService service = new SymbolPriceServiceImpl();

        Symbol price = service.getPrice("BTCUSDT");

        assertNotNull(price);

    }

    public void testGetPriceUnknow() {

        SymbolPriceService service = new SymbolPriceServiceImpl();

        Symbol price = service.getPrice("SYMBOL");

        assertNull(price);

    }
}
