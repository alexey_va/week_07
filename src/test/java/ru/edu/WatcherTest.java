package ru.edu;

import junit.framework.TestCase;
import ru.edu.model.Symbol;

import static org.mockito.Mockito.mock;

public class WatcherTest extends TestCase {
    private Symbol mockSymbol = mock(Symbol.class);
    private SymbolPriceService service = mock(SymbolPriceService.class);

    public void testNewWatcher() {

        Watcher newWatcher = new Watcher("Symbol", service);

        assertNotNull(newWatcher);

        newWatcher.run();
    }

    public void testNewWatcherIntegration() {

        SymbolPriceService serviceIntegration = new SymbolPriceServiceImpl();

        Watcher newWatcher = new Watcher("BNBBTC", serviceIntegration);

        assertNotNull(newWatcher);

        newWatcher.run();

    }
}
